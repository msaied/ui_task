# README #

* This is UI Task to determine your level as UI Developer

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### What Task Requirements? ###

* Implement task using Bootstrap framework.
* Using SASS instead CSS.
* Page to be responsive.
* header to be fixed on scroll down.
* Filter content using filter icons ( 5 icons ) , on click each icon will filter content below based on icon category.
* section ( What can DuHoot offer ) , circles to be tabs , on click each circle will change content below ( you can put dummy title and content ) for each tab.
* Page to be responsive.
* JS Validation on ( Subscribe to our newsletter ) , " Please Enter Valid Email".
* It should be delivered 

### Deployment Task Steps ###

* Clone from this url git clone git@bitbucket.org:msaied/ui_task.git 
* Checkout Branch That belong To you forexample will find Yournamebranch
* Convert PSD to HTML & CSS using based on above requirements
* Do your Work then Push on your branch

### Deadline for This Task ###
* It should be delivered before 10 July 2017.
* Any Push after this deadline will be automatically rejected 
* For anny questions just send me mail on mohamed.saied@parent.eu 


